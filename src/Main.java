import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

// Créer un client Pro test, pour montrer le résultat d'un autre test
        Pro clientProTest = new Pro("EKW12345678", "123456789", "Raison Sociale", 1500000, 1000, 500);
        double montantFactureProTest = clientProTest.calculerMontantFacture();

// Créer un particulier test, pour montrer le résultat d'un autre test
        Particulier particulierTest = new Particulier("EKW87654321", "M.", "Mouratoglou", "Patrick", 500, 200);
        double montantFactureParticulierTest = particulierTest.calculerMontantFacture();

// Demander de créer un client
        System.out.println("Créer un type de client :");
        System.out.println("1 - Professionnel");
        System.out.println("2 - Particulier");
        int choix;
        do{
            choix = sc.nextInt();
            if(choix == 1 ){
                System.out.println("Créer un client professionnel :");
                System.out.println("Saisir la référence du client :");
                String clientReference = sc.next();
                System.out.println("Saisir le numéro de SIRET :");
                String siret = sc.next();
                System.out.println("Saisir la raison sociale :");
                String raisonSociale = sc.next();
                System.out.println("Saisir le chiffre d'affaires :");
                double ca = sc.nextDouble();
                System.out.println("Saisir la consommation d'électricité :");
                int consommationElectricite = sc.nextInt();
                System.out.println("Saisir la consommation de gaz :");
                int consommationGaz = sc.nextInt();

                Pro clientPro = new Pro(clientReference, siret, raisonSociale, ca, consommationElectricite, consommationGaz);
                double montantFacturePro = clientPro.calculerMontantFacture();
                System.out.println("Montant facturé au client Pro : " + montantFacturePro);
                break;
            }
            else if(choix == 2){
                System.out.println("Créer un client particulier :");
                System.out.println("Saisir la référence du client :");
                String clientReference = sc.next();
                System.out.println("Saisir le titre :");
                String titre = sc.next();
                System.out.println("Saisir le nom :");
                String nom = sc.next();
                System.out.println("Saisir le prénom :");
                String prenom = sc.next();
                System.out.println("Saisir la consommation d'électricité :");
                int consommationElectricite = sc.nextInt();
                System.out.println("Saisir la consommation de gaz :");
                int consommationGaz = sc.nextInt();

                Particulier particulier = new Particulier(clientReference, titre, nom, prenom, consommationElectricite, consommationGaz);
                double montantFactureParticulier = particulier.calculerMontantFacture();
                System.out.println("Montant facturé au particulier : " + montantFactureParticulier);
                break;
            }
            else{
                System.out.println("Choix incorrect");
            }
        }while (choix != 1 || choix != 2);

// Afficher les montants facturés des autres tests
        System.out.println("----------------------------------------------");
        System.out.println("Montant facturé au client Pro de l'autre Test: " + montantFactureProTest);
        System.out.println("Montant facturé au particulier de l'autre Test: " + montantFactureParticulierTest);

    }
}