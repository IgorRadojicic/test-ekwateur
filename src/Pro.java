public class Pro extends Client{
    // Attributs
    private String siret;
    private String raisonSociale;
    int consommationElectricite;
    int consommationGaz;
    private double ca;

    // Constructeur
    public Pro(String clientReference, String siret, String raisonSociale, double ca, int consommationElectricite, int consommationGaz) {
        super(clientReference);
        this.siret = siret;
        this.raisonSociale = raisonSociale;
        this.ca = ca;
        this.consommationElectricite = consommationElectricite;
        this.consommationGaz = consommationGaz;
    }

    // Getters
    public double getCa() {
        return ca;
    }

    @Override
    public int getConsommationElectricite() {
        return consommationElectricite;
    }

    @Override
    public int getConsommationGaz() {
        return consommationGaz;
    }
}
