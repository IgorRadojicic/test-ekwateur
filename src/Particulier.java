public class Particulier extends Client {
    // Attributs
    private String civilite;
    private String nom;
    private String prenom;
    int consommationElectricite;
    int consommationGaz;

    // Constructeur
    public Particulier(String clientReference, String civilite, String nom, String prenom, int consommationElectricite, int consommationGaz) {
        super(clientReference);
        this.civilite = civilite;
        this.nom = nom;
        this.prenom = prenom;
        this.consommationElectricite = consommationElectricite;
        this.consommationGaz = consommationGaz;
    }

    // Getters
    @Override
    public int getConsommationElectricite() {
        return consommationElectricite;
    }

    @Override
    public int getConsommationGaz() {
        return consommationGaz;
    }
}
