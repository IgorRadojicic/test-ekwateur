public abstract class Client {
    private String clientReference;

    // Constructeur
    public Client(String clientReference){
        this.clientReference = clientReference;
    }

    // Getters
    public abstract int getConsommationElectricite();
    public abstract int getConsommationGaz();

    //Méthode pour calculer le montant de la facture zelon le type de client
    public double calculerMontantFacture() {
        double prixKwhElectricite;
        double prixKwhGaz;

        // Détermine le prix selon le type de client
        if (this instanceof Pro) {      // Pour les pros
            Pro clientPro = (Pro) this;
            if (clientPro.getCa() > 1000000) {
                prixKwhElectricite = 0.114;
                prixKwhGaz = 0.111;
            } else {
                prixKwhElectricite = 0.118;
                prixKwhGaz = 0.113;
            }
        } else {                        // Pour les particuliers
            prixKwhElectricite = 0.121;
            prixKwhGaz = 0.115;
        }

        // Calculer le montant pour l'électricité et le gaz
        double montantElectricite = getConsommationElectricite() * prixKwhElectricite;
        double montantGaz = getConsommationGaz() * prixKwhGaz;

        // Retourner le montant total à facturer
        return montantElectricite + montantGaz;
    }

}
